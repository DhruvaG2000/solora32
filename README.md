# SoLoRa32

A Charger PCB to power a LoRa32 device using Solar Energy and LiPo Battery.

## Collaborate on Trello
Use [this link](https://trello.com/b/bU0cZuUJ/cdp-roadmap) to view CDP roadmap.
This project can be tracked under Design/PCB Charger

## Test and Deploy

TODO

## Visuals

![](media/SoLoRa32_preview_R1.png)

## Installation
You will need KiCAD on your system (preferrably v5) and libraries are placed in this repo itself.

## Support
Please visit the CDP Slack community for support.

## Roadmap
- Add more LiPo Battery slots.
- Add on board sensors.
- Add more 3V3 and 5V and GND pins.

## Contributing
Just create a PR to this repo directly

## Resources
1. [Pinout](https://resource.heltec.cn/download/WiFi_LoRa_32/WIFI_LoRa_32_V2.pdf)
2. [Heltec LoRav2 Schematic](https://resource.heltec.cn/download/WiFi_LoRa_32/V2/WiFi_LoRa_32_V2(433%2C470-510).PDF)
3. [GreatScott! Solar Charging and Protection circuit V1](https://easyeda.com/editor#id=19d85904554e4ffea163c9be46c52993)
4. [GreatScott! Solar Charging and Protection circuit V2](file:///home/dhruva/Downloads/Schematic_DIY%20LiPo%20Supercharger%20Kit.pdf)

## License
MIT License

## Project status
WIP

